﻿using FAC;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FACTest
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		[DataRow(0.0001, 3.141592653589793e-8)]
		[DataRow(0.0010, 0.000003141592653589793)]
		[DataRow(0.0100, 0.0003141592653589793)]
		[DataRow(0.1000, 0.031415926535897934)]
		public void CycleAreaCalculatingForSmallValues(double x, double y)
		{
			var area = new Cycle { Radius = x }.GetArea();
			Assert.AreEqual(area, y, 0.0001);
		}

		[TestMethod]
		[DataRow(0.000, 0)]
		[DataRow(1.001, 3.1478789804896254)]
		[DataRow(10.70, 359.68094290949534)]
		[DataRow(100.9, 31983.957903593506)]
		public void CycleAreaCalculatingForMiddleValues(double x, double y)
		{
			var area = new Cycle { Radius = x }.GetArea();
			Assert.AreEqual(area, y, 0.0001);
		}

		[TestMethod]
		[DataRow(100213.12341235, 31549978621.33726)]
		[DataRow(1461233124534.3, 6.707935684421115e+24)]
		[DataRow(999999.99999999, 3141592653589.73)]
		[DataRow(256.12331235554, 206085.81127793455)]
		public void CycleAreaCalculatingForBigValues(double x, double y)
		{
			var area = new Cycle { Radius = x }.GetArea();
			Assert.AreEqual(area, y, 0.001);
		}

		[TestMethod]
		[DataRow(-1)]
		[DataRow(-2)]
		[DataRow(-100.912)]
		[ExpectedException(typeof(ArgumentException))]
		public void CycleAreaCalculatingForInvalidValue(double x)
		{
			var area = new Cycle { Radius = x }.GetArea();
		}

		[TestMethod]
		[DataRow(0.142, 0.124, 0.233, 0.007453)]
		public void TriangleAreaCalculatingForSmallValues(double a, double b, double c, double y)
		{
			var area = new Trianle { A = a, B = b, C = c }.GetArea();
			Assert.AreEqual(area, y, 0.0001);
		}

		[TestMethod]
		[DataRow(5, 4, 3, 6)]
		public void TriangleAreaCalculatingForMiddleValues(double a, double b, double c, double y)
		{
			var area = new Trianle { A = a, B = b, C = c }.GetArea();
			Assert.AreEqual(area, y, 0.0001);
		}

		[TestMethod]
		[DataRow(50000, 40000, 30000, 600000000)]
		public void TriangleAreaCalculatingForBigValues(double a, double b, double c, double y)
		{
			var area = new Trianle { A = a, B = b, C = c }.GetArea();
			Assert.AreEqual(area, y, 0.0001);
		}

		[TestMethod]
		[DataRow(-1, 1, 1)]
		[DataRow(1, -1, 1)]
		[DataRow(1, 1, -1)]
		[ExpectedException(typeof(ArgumentException))]
		public void TriangleAreaCalculatingForInvalidValue(double a, double b, double c)
		{
			var area = new Trianle { A = a, B = b, C = c }.GetArea();
			Assert.Fail();
		}

		[TestMethod]
		[DataRow(10, 11, 100)]
		[ExpectedException(typeof(InvalidOperationException))]
		public void TriangleAreaCalculatingForInvalidValues(double a, double b, double c)
		{
			var flag = new Trianle { A = a, B = b, C = c }.IsRigth();
			Assert.Fail();
		}

		[TestMethod]
		[DataRow(3, 4, 5, true)]
		[DataRow(10, 10, 14, false)]
		[DataRow(10, 11, 12, false)]
		public void TriangleRightness(double a, double b, double c, bool y)
		{
			var flag = new Trianle { A = a, B = b, C = c }.IsRigth();
			Assert.AreEqual(flag, y);
		}
	}
}
