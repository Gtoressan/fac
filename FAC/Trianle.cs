﻿using System;
using static System.Math;

namespace FAC
{
	public class Trianle : IAreaCalculatableFigure
	{
		double a = 0;
		double b = 0;
		double c = 0;

		/// <summary>
		/// The length of first side in meters.
		/// It is needed for calculating area.
		/// </summary>
		public double A
		{
			get => a;
			set => a = value >= 0 ? value : throw new ArgumentException("Side can't be negative.");
		}

		/// <summary>
		/// The length of second side in meters.
		/// It is needed for calculating area.
		/// </summary>
		public double B
		{
			get => b;
			set => b = value >= 0 ? value : throw new ArgumentException("Side can't be negative.");
		}

		/// <summary>
		/// The length of third side in meters.
		/// It is needed for calculating area.
		/// </summary>
		public double C
		{
			get => c;
			set => c = value >= 0 ? value : throw new ArgumentException("Side can't be negative.");
		}

		/// <summary>
		/// This method uses the <see cref="A"/>, <see cref="B"/>, <see cref="C"/> properties to calculating area.
		/// </summary>
		/// <returns>The area of triangle in meters.</returns>
		public double GetArea()
		{
			if (!Validate(A, B, C))
			{
				throw new InvalidOperationException();
			}

			// For calculating the are of trianle by its sides
			// we use Heron's formula.
			var p = (A + B + C) / 2;
			return Sqrt(p * (p - A) * (p - B) * (p - C));
		}

		/// <summary>
		/// This method uses the <see cref="A"/>, <see cref="B"/>, <see cref="C"/> properties to determine
		/// the rightness of trianle.
		/// </summary>
		/// <returns>Trianle rightness.</returns>
		public bool IsRigth()
		{
			if (!Validate(A, B, C))
			{
				throw new InvalidOperationException();
			}

			var a = Pow(A, 2);
			var b = Pow(B, 2);
			var c = Pow(C, 2);

			return a == b + c || b == a + c || c == a + b;
		}

		static bool Validate(double a, double b, double c)
		{
			if (a < b)
			{
				var temp = a;
				a = b;
				b = temp;
			}

			if (a < c)
			{
				var temp = a;
				a = c;
				c = temp;
			}

			return a < b + c;
		}
	}
}
