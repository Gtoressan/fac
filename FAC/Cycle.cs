﻿using System;
using static System.Math;

namespace FAC
{
	public class Cycle : IAreaCalculatableFigure
	{
		double radius = 0;

		/// <summary>
		/// The radius of the Cycle in meters.
		/// It is needed for calculating area.
		/// </summary>
		public double Radius
		{
			get => radius;
			set => radius = value >= 0 ? value : throw new ArgumentException("Radius can't be negative.");
		}

		/// <summary>
		/// This method uses the <see cref="Radius"/> property to calculating area.
		/// </summary>
		/// <returns>The area of cycle in meters</returns>
		public double GetArea()
		{

			return PI * Pow(Radius, 2);
		}
	}
}
