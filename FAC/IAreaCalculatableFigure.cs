﻿namespace FAC
{
	public interface IAreaCalculatableFigure
	{
		double GetArea();
	}
}
